//
//  UnresolvedViewController.swift
//  iglulabsTask
//
//  Created by Eshwar XpertizeIN on 20/06/18.
//  Copyright © 2018 Eshwar XpertizeIN. All rights reserved.
//

import UIKit
import SVProgressHUD

class UnresolvedViewController: UIViewController {
     @IBOutlet weak var categoryTableView: UITableView!
    var unresolvedlist = [Unresolved]()
    var user = [User]()
    override func viewDidLoad() {
        super.viewDidLoad()
       self.title = "UNRESOLVED LIST"
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
        ApiManager.shared.getData(completion: { list in
            self.unresolvedlist = list
            self.categoryTableView.reloadData()
             SVProgressHUD.dismiss()
            
        })
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
  
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UnresolvedViewController : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return unresolvedlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! UnresolvedTableViewCell
        
        cell.setUPUnCell(withModel: unresolvedlist[indexPath.row])
        cell.profileImage.layer.cornerRadius = cell.profileImage.frame.size.width / 2;
       
       // cell.profileImage.clipsToBounds = YES;
         cell.profileImage.layer.masksToBounds = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {


        self.performSegue(withIdentifier: "newsSegue", sender: indexPath.row)

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let index = sender as? Int {

            let deatailVC = segue.destination as! ShowBodyViewController

            deatailVC.subcat = unresolvedlist[index]

        }
    }
    
}
