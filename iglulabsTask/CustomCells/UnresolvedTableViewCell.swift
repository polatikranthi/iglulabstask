//
//  UnresolvedTableViewCell.swift
//  iglulabsTask
//
//  Created by Eshwar XpertizeIN on 20/06/18.
//  Copyright © 2018 Eshwar XpertizeIN. All rights reserved.
//

import UIKit
import SDWebImage
class UnresolvedTableViewCell: UITableViewCell {

    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
      //  self.profileImage.clipsToBounds = YES;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUPUnCell(withModel list:Unresolved )-> Void{
        
        
        titleLabel.text = list.title    
        bodyLabel.text = list.body
       
        if let URLString = list.user?.subcatimage {

            profileImage.sd_setImage(with: URL(string:URLString))


        }
        
    }
   
}
