//
//  ShowBodyViewController.swift
//  iglulabsTask
//
//  Created by Eshwar XpertizeIN on 20/06/18.
//  Copyright © 2018 Eshwar XpertizeIN. All rights reserved.
//

import UIKit
import SVProgressHUD
class ShowBodyViewController: UIViewController {
    var subcat  : Unresolved?
    @IBOutlet weak var dataWebView: UIWebView!

    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var asscationLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "USER DETAILS"
        // Do any additional setup after loading the view.
        titleLabel.text = subcat?.title
        
        if let htmlString = subcat?.body {
            SVProgressHUD.setDefaultMaskType(.clear)
            SVProgressHUD.show()
            self.dataWebView.loadHTMLString(htmlString,
                                                baseURL: nil)
             SVProgressHUD.dismiss()
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
