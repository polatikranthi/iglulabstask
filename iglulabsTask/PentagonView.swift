//
//  PentagonView.swift
//  iglulabsTask
//
//  Created by Eshwar XpertizeIN on 19/06/18.
//  Copyright © 2018 Eshwar XpertizeIN. All rights reserved.
//

import UIKit

class PentagonView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        backgroundColor = UIColor.clear
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        backgroundColor = UIColor.clear
//    }
//
//    override func draw(_ rect: CGRect) {
//        let size = self.bounds.size
//        let h = size.height * 0.85      // adjust the multiplier to taste
//
//        // calculate the 5 points of the pentagon
//        let p1 = self.bounds.origin
//        let p2 = CGPoint(x:p1.x + size.width, y:p1.y)
//        let p3 = CGPoint(x:p2.x, y:p2.y + h)
//        let p4 = CGPoint(x:size.width/2, y:size.height)
//        let p5 = CGPoint(x:p1.x, y:h)
//
//        // create the path
//        let path = UIBezierPath()
//        path.move(to: p1)
//        path.addLine(to: p2)
//        path.addLine(to: p3)
//        path.addLine(to: p4)
//        path.addLine(to: p5)
//        path.close()
//
//        // fill the path
//        UIColor.red.set()
//        path.fill()
//    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: (rect.maxX / 2.0), y: rect.minY))
        context.closePath()
        
        context.setFillColor(red: 1.0, green: 0.5, blue: 0.0, alpha: 0.60)
        context.fillPath()
    }
}
